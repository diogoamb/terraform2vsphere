# Terraform

Este projeto tem amostras básicas para implantar recursos no vSphere usando os arquivos de configuração do Terraform.

Este conteudo foi escrito através deste [Post](https://medium.com/@amolkokje/infrastructure-as-a-code-using-terraform-aws-vsphere-8878ef127205).

Por favor, especifique os valores para as seguintes variáveis no arquivo terraform.tfvars na mesma pasta que os arquivos .tf.
A descrição para as variáveis é dada no arquivo variables.tf. Se não for especificado, o Terraform solicitará que você insira os valores ao aplicar suas alterações.

## vSphere

### Credentials for vSphere
vsphere_user = <> 
vsphere_password = <> 
vsphere_vcenter = <> 
vsphere_unverified_ssl = <> 

### VM Specifications
vsphere_datacenter = <> 
vsphere_vm_name = <> 
vsphere_vm_template = <> 
vsphere_vcpu_number = <> 
vsphere_memory_size = <> 
vsphere_datastore = <> 
vsphere_ipv4_address = <> 
vsphere_dns_servers = <> 
vsphere_domain = <> 
vsphere_time_zone = <> 
vsphere_port_group = <> 


### Inicialização:
O Terraform usa uma arquitetura baseada em plug-in para suportar os inúmeros fornecedores de infraestrutura e serviços disponíveis. Como na Terraform versão 0.10.0, cada “Provider” é seu próprio binário encapsulado distribuído separadamente do próprio Terraform.

O comando "init" baixará e instalará automaticamente qualquer binário do provedor para os provedores em uso na configuração.

```
terraform init
```
Se a configuração do seu provedor for AWS (provider “aws”), ele fará o download do plug-in e será autenticado com a AWS. Ele fará o mesmo, se for um provedor diferente, digamos vSphere (provedor “vsphere”).


###  Aplicar mudanças:
Depois de ter configurado sua configuração e construir a infraestrutura, o Terraform é capaz de rastrear seus recursos. Se você estava usando scripts antes, sabe o quanto foi difícil controlar e gerenciar os recursos ou até mesmo excluí-los. Terraform rastreia as mudanças que você fez, e também é capaz de derrubar toda a sua infraestrutura usando um único comando.
 
Se você quiser ver quais alterações na configuração serão aplicadas à sua infraestrutura, use o comando:
```
terraform plan
```
Para aplicar as alterações à sua infraestrutura, use o comando "apply". Isso criará a infraestrutura que você especificou em seus arquivos de configuração. Se houver alterações feitas na infraestrutura existente em seus arquivos de configuração, "apply" atualizará sua infraestrutura de acordo.

```
terraform apply
```
Depois de implantado, você poderá ver sua implantação usando o comando "show":

```
terraform show 
```

Depois que a implantação é concluída, um arquivo .tfstate é criado. Este é um arquivo de estado é extremamente importante; Ele rastreia os IDs dos recursos criados para que o Terraform saiba o que está gerenciando.

### Excluir:

Para decompor ou destruir a infraestrutura que você implantou, você pode usar o comando "destroy". Não é necessário escrever um script para encontrar VMs com nomes específicos, em locais de pastas, com tags, etc.

```
terraform destroy
```


